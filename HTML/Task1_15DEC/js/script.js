let loginButton = document.getElementById("login_btn");
if (loginButton)
	loginButton.addEventListener("click", function (e) {
		e.preventDefault();
		// alert("login done")
		const userName = document.getElementById("username").value;
		const password = document.getElementById("password").value;
		console.log(`username: ${userName} & password: ${password}`);
		alert(`username: ${userName} & password: ${password}`);
	});

let registerButton = document.getElementById("register-btn");
if (registerButton)
	registerButton.addEventListener("click", function (e) {
		e.preventDefault();
		// alert("login done")
		const userName1 = document.getElementById("username").value;
		const password1 = document.getElementById("password").value;
		const emailId = document.getElementById("email").value;
		const mobileNo = document.getElementById("mobileNo").value;

		user = {
			userName1: userName1,
			password1: password1,
			emailId: emailId,
			mobileNo: mobileNo,
		};
		var registerUsers = JSON.parse(localStorage.getItem("registerUsers"));
		if (registerUsers) {
			registerUsers.push(user);
			localStorage.setItem("registerUsers", JSON.stringify(registerUsers));
		} else {
			users = [];
			users.push(user);
			localStorage.setItem("registerUsers", JSON.stringify(users));
		}

		console.log(user, registerUsers, localStorage.getItem("registerUsers"));
		console.log(
			`username: ${userName1}, password: ${password1},Email: ${emailId}, Mobile no.: ${mobileNo}`
		);
		alert(
			`username: ${userName1}, password: ${password1},Email: ${emailId}, Mobile no.: ${mobileNo}`
		);
	});

let addMessageButton = document.getElementById("addMessage_btn");
if (addMessageButton)
	addMessageButton.addEventListener("click", function (e) {
		// e.preventDefault();
		// alert("login done")
		const messageText = document.getElementById("message").value;
		let parentDiv = document.getElementById("output-container");
		var div = document.createElement("div");
		div.className = "output";

		let p = document.createElement("p");
		p.innerText = messageText;

		div.appendChild(p);

		parentDiv.appendChild(div);
		console.log(parentDiv);
		console.log(div);

		console.log("message: ", messageText);
		// alert("message: "+messageText);
	});

function getRegisterUsers() {
	let savedUsers = JSON.parse(localStorage.getItem("registerUsers"));
	console.log(savedUsers);

	savedUsers.forEach((user, index) => {
		console.log(user);
		var table = document.getElementById("myTable");

		var row = table.insertRow();

		var tId = row.insertCell(0);
		var tUsername = row.insertCell(1);
		var tEmail = row.insertCell(2);
		var tMobile = row.insertCell(3);
		var tAction = row.insertCell(4);

		tId.innerText = index + 1;
		tUsername.innerText = user.userName1;
		tEmail.innerText = user.emailId;
		tMobile.innerText = user.mobileNo;
		tAction.innerHTML = `<a href='#${index+1}'>Delete</a>`;

		//   cell1.innerHTML = "NEW CELL1";
		//   cell2.innerHTML = "NEW CELL2";
	});
}
